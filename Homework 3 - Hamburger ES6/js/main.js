// Hamburger class

class Hamburger {
  constructor(size, stuffing) {
    try {
      if (arguments.length === 0 || arguments.length > 2)
        throw new HamburgerException('Wrong number of argument');
      if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE)
        throw new HamburgerException('No size argument')
      if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_SALAD && stuffing !== Hamburger.STUFFING_POTATO)
        throw new HamburgerException('No stuffing argument')

      this.size = size;
      this.stuffing = stuffing;
      this.toppings = [];

    } catch(error) {
      console.log(error);
    }
  }

  addTopping() {
    try {
      if (arguments.length === 0)
        throw new HamburgerException('No toppings added');
      for (let i = 0; i < arguments.length; i++)
        if (arguments[i] !== Hamburger.TOPPING_MAYO && arguments[i] !== Hamburger.TOPPING_SPICE)
          throw new HamburgerException('Wrong topping arguemt');
      for (let i = 0; i < arguments.length; i++)
        if (this.toppings.includes(arguments[i]))
          throw new HamburgerException('topping ' + arguments[i].topping + ' has been added before');
      for (let i = 0; i < arguments.length; i++)
        for (let j = 0; j < arguments.length; j++)
          if (arguments[i] === arguments[j] && i !== j)
            throw new HamburgerException('You can add only one topping of each kind');
  
      for (let i = 0; i < arguments.length; i++)
        this.toppings.push(arguments[i]);
  
    } catch(error) {
      console.log(error);
    }
  }

  removeTopping(topping) {
    try {
      if (arguments.length > 1 || arguments.length === 0)
        throw new HamburgerException('Wrong number of toppings');
      if (!this.toppings.includes(topping))
        throw new HamburgerException('Hamburger object does not have ' + topping.topping + ' topping to remove');
  
      this.toppings.splice(this.toppings.indexOf(topping), 1);
  
    } catch (error) {
      console.log(error);
    }
  }
  
  getToppings() {
    return this.toppings;
  }
  
  getSize() {
    return this.size.size;
  }
  
  getStuffing() {
    return this.stuffing.stuffing;
  }
  
  calculatePrice() {
    let sizePrice = this.size.price;
    let stuffingPrice = this.stuffing.price;
    let toppingsPrice = this.toppings.reduce(function(sum, topping) {
      return sum + topping.price;
    }, 0);
  
    return sizePrice + stuffingPrice + toppingsPrice;
  }
  
  calculateCalories() {
    let toppingsCalories = this.toppings.reduce(function(sum, topping) {
      return sum + topping.calories;
    }, 0);
  
    return toppingsCalories + this.size.calories + this.stuffing.calories;
  }

  // Hamburger parameters
  static SIZE_SMALL = {size: 'small', price: 50, calories: 20};
  static SIZE_LARGE = {size: 'large', price: 100, calories: 40};
  static STUFFING_CHEESE = {stuffing: 'cheese', price: 10, calories: 20};
  static STUFFING_SALAD = {stuffing: 'salad', price: 20, calories: 5};
  static STUFFING_POTATO = {stuffing: 'potato', price: 15, calories: 10};
  static TOPPING_MAYO = {topping: 'mayo', price: 20, calories: 5};
  static TOPPING_SPICE = {topping: 'spice', price: 15, calories: 0};
}

// Hamburger exception
class HamburgerException extends Error {
  constructor(message) {
    super();
    this.name = 'Hamburger Exception';
    this.message = message;
  }
}

// create hamburger
let h = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

// test hamberger
h.addTopping(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE);

console.log('toppings:', h.getToppings());
console.log('size: ' + h.getSize());
console.log('stuffing: ' + h.getStuffing());
console.log('price: ' + h.calculatePrice() + ' UAH');
console.log('calories: ' + h.calculateCalories());

h.removeTopping(Hamburger.TOPPING_SPICE);
console.log('remaining toppings:', h.getToppings());