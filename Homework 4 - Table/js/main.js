// generate and append table
const table = document.createElement('table');
const body = document.body;

for (let i = 0; i < 30; i++) {
  const row = document.createElement('tr');
  for (let i = 0; i < 30; i++) {
    const cell = document.createElement('td');
    row.appendChild(cell);
  }
  table.appendChild(row);
}

body.appendChild(table);

// change colors
body.addEventListener('click', event => {
  const targetEl = event.target;
  const eventHolder = event.currentTarget;

  if (targetEl.tagName === 'TD')
    targetEl.classList.toggle('fill');

  if (targetEl === eventHolder) {
    const table = document.getElementsByTagName('table')[0];
    table.classList.toggle('color-reverse');
  }
});